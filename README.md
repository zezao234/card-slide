# Card Sliding
A simple card sliding

## Installing
Simply run

    npm install

## Acessing
Initialize card server and our server

    npm start

and access ``http://localhost:8080/index.html``

>If you want to access the data you can go to  ``http://127.0.0.1:3000/cards``

> If you want to retrieve just a subset, specify the lower and upper limit as query parameters, for example: ``http://127.0.0.1:3000/cards?_start=8&_end=12``


## Running the tests

![protractor]( https://deerawan.gallerycdn.vsassets.io/extensions/deerawan/vscode-protractor-snippets/1.0.5/1474455426693/Microsoft.VisualStudio.Services.Icons.Default  "Protractor")

The automated tests use [Protractor](http://www.protractortest.org/#/), with jasmine and selenium, which is a framework
for AngularJS apps, but we disabled the AngularJS part for these tests.

We test on Chrome and Firefox.
### What are we testing

We test the specs the designing team gave us (margin, padding...), as well
the number of cards that are displayed after each animation

    e.g : "Expect margin-left to be 19px"

### Prerequisites

We need to install Protractor to run our test files. Simple run:

    npm install -g protractor

and update

    webdriver-manager update

### Run the tests

Start the Selenium Server with

    webdriver-manager start

And your test configuration file (e.g: _conf.js_) file through your IDE (Webstorm allows Protractor) or simply run

    protractor conf.js

### Notes

We have two configuration files, one for Large and one for XS (since our site
only changes with these dimensions).

For more information about Protractor Tests check the [Table of Contents](http://www.protractortest.org/#/toc)
