var cardSlide = function () {
    "use strict";

    //Links
    const cardLink = 'http://localhost:8080/index.html';

    //Elements
    const body = $('body');
    const firstContainer = $('.container:first-child');
    const cardElements = element.all(by.className('card'));
    const cardFirstElement = $('.card:first-child');
    const cardSecondElement = $('.card:nth-child(2)');
    const cardInner = $('.card-inner');
    const cardTop = $('.card-top');
    const cardTitle = $('.card-title');
    const cardSubtitle = $('.card-subtitle');
    const cardInnerText = $('.card-text');
    const cardLearnMore = $('.card-learn-more');
    const arrowsDivElement = $('#arrows');
    const leftArrow = $('#left-arrow');
    const rightArrow = $('#right-arrow');

    //Values
    const browserTitle = 'Mindera cards';
    const numberOfCardsVisible = 3;
    const cardMargin = '26px';
    const containerPadding = '26px';
    const cardWidth = '308px';
    const cardInnerPadding = '19px';
    const arrowsWidth = '40px';
    const cardFontFamily = 'Arial';
    const cardFontWeightBoldChrome = 'bold';
    const cardFontWeightNormalChrome = 'normal';
    const cardFontWeightBoldFirefox = '700';
    const cardFontWeightNormalFirefox = '400';
    const bigTextSize = '23px';
    const mediumTextSize = '16px';
    const smallTextSize = '12px';
    const innerTextLineHeight = 1.583;
    const textColorChrome = 'rgba(58, 58, 58, 1)';
    const textColorFirefox = 'rgb(58, 58, 58)';
    const subtitleColorChrome = 'rgba(173, 173, 173, 1)';
    const subtitleColorFirefox = 'rgb(173, 173, 173)';
    const bodyBackgroundColorChrome = 'rgba(246, 246, 246, 1)';
    const bodyBackgroundColorFirefox = 'rgb(246, 246, 246)';
    const greenColorChrome = 'rgba(45, 169, 54, 1)';
    const greenColorFirefox = 'rgb(45, 169, 54)';

    const sleepTime = 3500;

    /*===================
            GETTERS
    =====================*/

    // LINKS
    this.getCardLink = function () {
        return cardLink;
    };

    // ELEMENTS

    this.getBody = function() {
      return body;
    };

    this.getFirstContainer  = function() {
       return firstContainer;
    };

    this.getCardElements = function() {
        return cardElements;
    };

    this.getCardFirstElement = function() {
      return cardFirstElement;
    };

    this.getCardSecondElement = function() {
      return cardSecondElement;
    };

    this.getCardInner = function() {
      return cardInner;
    };

    this.getCardTop = function() {
      return cardTop;
    };

    this.getCardTitle = function() {
      return cardTitle;
    };

    this.getCardSubtitle = function() {
      return cardSubtitle;
    };

    this.getCardInnerText = function() {
      return cardInnerText;
    };

    this.getCardLearnMore = function() {
      return cardLearnMore;
    };

    this.getArrowsDivElement = function() {
      return arrowsDivElement;
    };

    this.getLeftArrow = function() {
      return leftArrow;
    };

    this.getRightArrow = function() {
      return rightArrow;
    };

    // VALUES

    this.getBrowserTitle = function() {
      return browserTitle;
    };

    this.getNumberOfCardsVisible = function() {
        return numberOfCardsVisible;
    };

    this.getCardMargin = function() {
        return cardMargin;
    };

    this.getContainerPadding = function() {
       return containerPadding;
    };

    this.getCardWidth = function() {
      return cardWidth;
    };

    this.getCardInnerPadding = function() {
        return cardInnerPadding;
    };

    this.getArrowsWidth = function() {
      return arrowsWidth;
    };

    this.getCardFontFamily = function() {
      return cardFontFamily;
    };

    this.getCardFontWeightBoldChrome = function() {
      return cardFontWeightBoldChrome;
    };

    this.getCardFontWeightNormalChrome = function() {
      return cardFontWeightNormalChrome;
    };

    this.getCardFontWeightBoldFirefox = function() {
      return cardFontWeightBoldFirefox;
    };

    this.getCardFontWeightNormalFirefox = function() {
      return cardFontWeightNormalFirefox;
    };

    this.getBigTextSize = function() {
      return bigTextSize;
    };

    this.getMediumTextSize = function() {
      return mediumTextSize;
    };

    this.getSmallTextSize = function() {
      return smallTextSize;
    };

    this.getInnerTextLineHeightChrome = function() {
      return innerTextLineHeight * parseInt(smallTextSize.split('px').join('')) +'px';
    };

    this.getInnerTextLineHeightFirefox = function() {
      return Math.round(innerTextLineHeight * parseInt(smallTextSize.split('px').join(''))) +'px';
    };


    this.getTextColorChrome = function() {
      return textColorChrome;
    };

    this.getTextColorFirefox = function() {
      return textColorFirefox;
    };

    this.getSubtitleColorChrome = function() {
      return subtitleColorChrome;
    };

    this.getSubtitleColorFirefox = function() {
      return subtitleColorFirefox;
    };

    this.getBodyBackgroundColorChrome = function() {
      return bodyBackgroundColorChrome;
    };

    this.getBodyBackgroundColorFirefox = function() {
      return bodyBackgroundColorFirefox;
    };

    this.getGreenColorChrome = function() {
      return greenColorChrome;
    };

    this.getGreenColorFirefox = function() {
      return greenColorFirefox;
    };

    this.getSleepTime = function() {
        return sleepTime;
    }
};

module.exports = new cardSlide();