var cardSlide = require('../../page/cardSlidePage.js');

describe('card Slide', function () {

    // browser.driver.manage().window().maximize();

    describe('outside card', function () {
        it('should go to url page', function () {
            browser.get(cardSlide.getCardLink());
            expect(browser.getCurrentUrl()).toBe(cardSlide.getCardLink());
        });

        //Correct problem with card loading
        browser.sleep(200);

        it('should have title name', function () {
            expect(browser.getTitle()).toBe(cardSlide.getBrowserTitle());
        });

        it('should have color on background', function () {
            expect(cardSlide.getBody().getCssValue('background-color')).toBe(cardSlide.getBodyBackgroundColorFirefox());
        });

        it('should have cards visible', function () {
            expect(cardSlide.getCardElements().isPresent()).toBeTruthy();
        });

        it('should have container padding-top', function () {
            expect(cardSlide.getFirstContainer().getCssValue('padding-top')).toBe(cardSlide.getContainerPadding());
        });

        it('should have container padding-bottom', function () {
            expect(cardSlide.getFirstContainer().getCssValue('padding-bottom')).toBe(cardSlide.getContainerPadding());
        });

        it('should have container padding-left', function () {
            expect(cardSlide.getFirstContainer().getCssValue('padding-left')).toBe(cardSlide.getContainerPadding());
        });

        it('should have X width on arrows', function () {
            expect(cardSlide.getArrowsDivElement().getCssValue('width')).toBe(cardSlide.getArrowsWidth());
        });

        it('should have X cards', function () {
            expect(cardSlide.getCardElements().count()).toBe(cardSlide.getNumberOfCardsVisible());
        });

        it('should have no margin-left on first card', function () {
            expect(cardSlide.getCardFirstElement().getCssValue('margin-left')).toBe('0px');
        });

        it('should have margin-left on second card', function () {
            expect(cardSlide.getCardSecondElement().getCssValue('margin-left')).toBe(cardSlide.getCardMargin());
        });

        it('should have X card width', function () {
            expect(cardSlide.getCardFirstElement().getCssValue('width')).toBe(cardSlide.getCardWidth());
        });

    });

    describe('inside card', function () {

        it('should have inner padding-top', function () {
            expect(cardSlide.getCardInner().getCssValue('padding-top')).toBe(cardSlide.getCardInnerPadding());
        });

        it('should have inner padding-right', function () {
            expect(cardSlide.getCardInner().getCssValue('padding-right')).toBe(cardSlide.getCardInnerPadding());
        });

        it('should have inner padding-bottom', function () {
            expect(cardSlide.getCardInner().getCssValue('padding-bottom')).toBe(cardSlide.getCardInnerPadding());
        });

        it('should have inner padding-left', function () {
            expect(cardSlide.getCardInner().getCssValue('padding-left')).toBe(cardSlide.getCardInnerPadding());
        });

        it('should have margin bottom on titles div', function () {
            expect(cardSlide.getCardTop().getCssValue('margin-bottom')).toBe(cardSlide.getCardInnerPadding());
        });

        it('should have margin bottom on text div', function () {
            expect(cardSlide.getCardInnerText().getCssValue('margin-bottom')).toBe(cardSlide.getCardInnerPadding());
        });

        it('should have title correct size', function () {
            expect(cardSlide.getCardTitle().getCssValue('font-size')).toBe(cardSlide.getBigTextSize());
        });

        it('should have subtitle correct size', function () {
            expect(cardSlide.getCardSubtitle().getCssValue('font-size')).toBe(cardSlide.getSmallTextSize());
        });

        it('should have inner text correct size', function () {
            expect(cardSlide.getCardInnerText().getCssValue('font-size')).toBe(cardSlide.getSmallTextSize());
        });

        it('should have learn more correct size', function () {
            expect(cardSlide.getCardLearnMore().getCssValue('font-size')).toBe(cardSlide.getMediumTextSize());
        });

        it('should have correct font-family on title', function () {
            expect(cardSlide.getCardTitle().getCssValue('font-family')).toBe(cardSlide.getCardFontFamily());
        });

        it('should have correct font-family on subtitle', function () {
            expect(cardSlide.getCardSubtitle().getCssValue('font-family')).toBe(cardSlide.getCardFontFamily());
        });

        it('should have correct font-family on inner text', function () {
            expect(cardSlide.getCardInnerText().getCssValue('font-family')).toBe(cardSlide.getCardFontFamily());
        });

        it('should have correct font-family on learn more text', function () {
            expect(cardSlide.getCardLearnMore().getCssValue('font-family')).toBe(cardSlide.getCardFontFamily());
        });

        it('should have correct font-weight on title', function () {
            expect(cardSlide.getCardTitle().getCssValue('font-weight')).toBe(cardSlide.getCardFontWeightBoldFirefox());
        });

        it('should have correct font-weight on subtitle', function () {
            expect(cardSlide.getCardSubtitle().getCssValue('font-weight')).toBe(cardSlide.getCardFontWeightBoldFirefox());
        });

        it('should have correct font-weight on inner text', function () {
            expect(cardSlide.getCardInnerText().getCssValue('font-weight')).toBe(cardSlide.getCardFontWeightNormalFirefox());
        });

        it('should have correct font-weight on learn more text', function () {
            expect(cardSlide.getCardLearnMore().getCssValue('font-weight')).toBe(cardSlide.getCardFontWeightBoldFirefox());
        });

        it('should have correct line-weight on inner text',function() {
            expect(cardSlide.getCardInnerText().getCssValue('line-height')).toBe(cardSlide.getInnerTextLineHeightFirefox());
        });

        it('should have correct text-color on title', function () {
            expect(cardSlide.getCardTitle().getCssValue('color')).toBe(cardSlide.getTextColorFirefox());
        });

        it('should have correct text-color on subtitle', function () {
            expect(cardSlide.getCardSubtitle().getCssValue('color')).toBe(cardSlide.getSubtitleColorFirefox());
        });

        it('should have correct text-color on inner text', function () {
            expect(cardSlide.getCardInnerText().getCssValue('color')).toBe(cardSlide.getTextColorFirefox());
        });

        it('should have correct text-color on learn more text', function () {
            expect(cardSlide.getCardLearnMore().getCssValue('color')).toBe(cardSlide.getGreenColorFirefox());
        });


    });

    describe('after slide right arrow', function () {

        it('should apply animation and number of cards should be equal', function () {
            cardSlide.getRightArrow().click().then( function(){browser.sleep(cardSlide.getSleepTime())});
            expect(cardSlide.getCardElements().count()).toBe(cardSlide.getNumberOfCardsVisible());
        });
    });

    describe('after slide left arrow', function() {
        it('should apply animation and number of cards should be equal', function () {
            cardSlide.getLeftArrow().click().then( function(){browser.sleep(3000)});
            expect(cardSlide.getCardElements().count()).toBe(cardSlide.getNumberOfCardsVisible());
        });
    });
});