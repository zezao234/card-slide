(function () {
    "use strict";

    const numberOfCards = 3;    //Number of cards to hopefully load. Fixed value
    var cardsToLoad = numberOfCards; //Number of cards to actually load.
    var data; //Data to retrieve from server
    var currentIndex = 0; //Index we are on our data array
    var url = 'http://127.0.0.1:3000/cards'; //Url for the GET request
    var hrefLink = 'https://mindera.com'; //href for the "learn more" button. We'll append the data id
    var keyframes = findKeyFrameRule('card-slide'); //To animate our slide. Will change rules later
    var original; //Variable to store original element to copy
    var translateValue; //The value for the keyframe rule
    var cardMargin = findPropertyBySelectorText('.container .row .card + .card', 'marginLeft').split('px').join(''); //Get card Margin (since it's fixed) for later fix os elements
    var cardWidth; //Will store card width
    var extra = 5; //Extra px to ensure elements are right
    var indexForOffsetTop = currentIndex; //Will be used when keyframe has translateY

    /**
     *@description Retrieves data from GET request, clones the number of cards we want, resize elements and checks which arrows are clickable
     */
    document.addEventListener("DOMContentLoaded", function (event) {

        //GET information from url
        getRequest(url).then(
            function (resolve) {
                data = resolve.responseText;
                data = JSON.parse(data);

                original = document.getElementsByClassName('row')[0].firstElementChild;

                //Prevent outOfBounds error
                if (currentIndex + cardsToLoad > data.length) {
                    cardsToLoad = data.length;

                    //If it's the end already we can't swipe
                    document.getElementById('right-arrow').className += ' disabled';
                }

                //Insert elements
                duplicateEnd(original, data, currentIndex, cardsToLoad);

                currentIndex = cardsToLoad;

                original.remove();
                document.getElementById('left-arrow').className += ' disabled';

                cardWidth = document.getElementsByClassName('card')[0].offsetWidth;
                resizeContainer();

                document.getElementById('loader').style.display = 'none';

            },
            function (error) {
                //Display error message
                document.getElementById('error').style.display = 'block';
                document.getElementById('pagebody').style.display = 'none';


                document.getElementById('loader').style.display = 'none';
            }
        );

    });

    /**
     * @description when resizing window fix elements widths and heights
     */
    window.addEventListener('resize', function () {
        resizeContainer();
    });


    function leftArrowSlide() {
        //If doesn't have our disabled class and isn't sliding
        if (document.getElementById('left-arrow').className.indexOf('disabled') === -1 && document.getElementsByClassName('row')[0].className.indexOf('card-translate') === -1) {

            var check = 0; //To fix index

            if (currentIndex - numberOfCards <= 0) {
                cardsToLoad = currentIndex;
            }
            else if (numberOfCards > currentIndex - numberOfCards) {
                cardsToLoad = currentIndex - numberOfCards;
                currentIndex = currentIndex - numberOfCards;
                check = 1;

            } else {
                cardsToLoad = numberOfCards;
                currentIndex -= numberOfCards;
            }

            if (cardsToLoad > 0) {
                document.getElementById('right-arrow').className = document.getElementById('right-arrow').className.replace(' disabled', '');
            }

            original = document.getElementsByClassName('row')[0].firstElementChild;

            duplicateBeginning(original, data, currentIndex, cardsToLoad);

            if (window.innerWidth <= 576) {

                //For better visual effect
                for (var i = 0; i < currentIndex; i++) {
                    document.getElementsByClassName('card')[i].style.display = 'none';
                }


                //When images finish loading we start, else it would give wrong offset value
                document.getElementsByClassName('card-top-image')[0].getElementsByTagName('img')[0].onload = function () {

                    for (var i = 0; i < currentIndex; i++) {
                        document.getElementsByClassName('card')[i].style.display = '';
                    }

                    //Get offset from correct Element
                    if (currentIndex + numberOfCards >= data.length) {
                        translateValue = document.getElementsByClassName('card')[currentIndex - 1].offsetTop;
                    } else {
                        translateValue = document.getElementsByClassName('card')[currentIndex].offsetTop;
                    }


                    alterRuleTranslation(keyframes, '0%', -translateValue, 'translateY');
                    alterRuleTranslation(keyframes, '100%', 0, 'translateY');

                    //Add our class which uses the keyframe
                    original.parentNode.className += ' card-translate';


                    //For doing the transition end only once
                    var transitionControl = 0;

                    document.getElementsByClassName('row')[0].addEventListener('animationend', function () {
                        if (transitionControl === 0) {
                            document.getElementsByClassName('row')[0].className = document.getElementsByClassName('row')[0].className.replace(' card-translate', '');
                            document.getElementsByClassName('row')[0].style.left = null;
                            document.getElementsByClassName('row')[0].style.top = null;
                            removeItem(document.getElementsByClassName('card'), cardsToLoad);
                            resizeContainer();
                        }
                        transitionControl++;
                    });

                    if (currentIndex - cardsToLoad <= 0) {
                        document.getElementById('left-arrow').className += ' disabled';
                    }

                };

            }

            else {
                resizeContainer();

                //Alter keyframe value to translate the width of the number of cards we load + the margin
                translateValue = getTranslateValue(cardsToLoad, extra);


                original.parentNode.style.left = -(+cardMargin * (cardsToLoad - 1) + cardWidth * cardsToLoad) + 'px';

                //Override keyframes
                alterRuleTranslation(keyframes, '0%', 0, 'translateX');
                alterRuleTranslation(keyframes, '100%', translateValue, 'translateX');

                //Add our class which uses the keyframe
                original.parentNode.className += ' card-translate';

                //For doing the transition end only once
                var transitionControl = 0;

                document.getElementsByClassName('row')[0].addEventListener('animationend', function () {
                    if (transitionControl === 0) {
                        document.getElementsByClassName('row')[0].className = document.getElementsByClassName('row')[0].className.replace(' card-translate', '');
                        document.getElementsByClassName('row')[0].style.left = null;
                        document.getElementsByClassName('row')[0].style.top = null;
                        removeItem(document.getElementsByClassName('card'), cardsToLoad);
                        resizeContainer();
                    }
                    transitionControl++;
                });

                //Fix index
                if (check === 1) {
                    currentIndex = numberOfCards;
                    document.getElementById('left-arrow').className += ' disabled';
                }

                if (currentIndex - cardsToLoad <= 0) {
                    document.getElementById('left-arrow').className += ' disabled';
                }
            }
        }
    }

    /**
     * @description when clicking the left arrow slides to left if screen width < 576px, else slides down
     */
    document.getElementById('left-arrow').addEventListener('click', leftArrowSlide());


    /**
     * @description when clicking the right arrow slides to right if screen width < 576px, else slides up
     */
    document.getElementById('right-arrow').addEventListener('click',rightArrowSlide);

    function rightArrowSlide() {
        //If doesn't have our disabled class and isn't sliding
        if (document.getElementById('right-arrow').className.indexOf('disabled') === -1 && document.getElementsByClassName('row')[0].className.indexOf('card-translate') === -1) {

            if (currentIndex + numberOfCards >= data.length) {
                cardsToLoad = data.length - currentIndex;
                document.getElementById('right-arrow').className += ' disabled';

                indexForOffsetTop = cardsToLoad;
            }
            else {
                cardsToLoad = numberOfCards;
                indexForOffsetTop = cardsToLoad;
            }

            if (cardsToLoad > 0) {
                document.getElementById('left-arrow').className = document.getElementById('left-arrow').className.replace(' disabled', '');
            }

            original = document.getElementsByClassName('row')[0].firstElementChild;

            //duplicate elements and append them to the end
            duplicateEnd(original, data, currentIndex, cardsToLoad);

            if (window.innerWidth <= 576) {
                translateValue = document.getElementsByClassName('card')[indexForOffsetTop].offsetTop;

                alterRuleTranslation(keyframes, '0%', 0, 'translateY');
                alterRuleTranslation(keyframes, '100%', -translateValue, 'translateY');
            }
            else {
                resizeContainer();
                //Alter keyframe value to translate the width of the number of cards we load + the margin
                translateValue = getTranslateValue(cardsToLoad);

                //Override keyframes
                alterRuleTranslation(keyframes, '0%', 0, 'translateX');
                alterRuleTranslation(keyframes, '100%', -translateValue, 'translateX');
            }

            //Add our class which uses the keyframe
            original.parentNode.className += ' card-translate';

            //For doing the transition end only once
            var transitionControl = 0;

            document.getElementsByClassName('row')[0].addEventListener('animationend', function () {
                if (transitionControl === 0) {
                    document.getElementsByClassName('row')[0].className = document.getElementsByClassName('row')[0].className.replace(' card-translate', '');
                    removeItem(document.getElementsByClassName('card'), cardsToLoad, 0);
                    resizeContainer();
                }
                transitionControl++;
            });

            if (window.innerWidth <= 576) {
                if (document.getElementById('right-arrow').className.indexOf('disabled') !== -1) {
                    document.getElementsByClassName('container')[0].style.height = document.getElementsByClassName('container')[0].offsetHeight + 'px';
                }
            }
            currentIndex += cardsToLoad;


        }

    }


    /**
     *
     * @param e - Key pressed on the html page
     * @description catch event when key is pressed and moves card slide accordingly
     */
    document.onkeydown = function (e) {
        if (e.keyCode === 37) {
            leftArrowSlide();
        }
        else if (e.keyCode === 39) {
            rightArrowSlide();
        }
    };

    /**
     * @description Resize Elements height and width because of positions relative and absolute and different screen widths
     */

    //TODO: There's a problem when resizing from XS to SM+ during sliding
    function resizeContainer() {


        //Prevent resizing while slide is active. Would cause problems
        if (document.getElementsByClassName('row')[0].className.indexOf('card-translate') === -1) {


            var containerElement = document.getElementsByClassName('container')[0];
            var rowElement = document.getElementsByClassName('row')[0];
            var cardElement = document.getElementsByClassName('card')[0];


            // //If screen XS
            if (window.innerWidth <= 576) {
                containerElement.style.width = cardElement.offsetWidth + 'px';
                rowElement.style.width = cardElement.offsetWidth + 'px';
                document.getElementsByClassName('row')[1].style.width = cardElement.offsetWidth + 'px';

                containerElement.style.height = rowElement.offsetHeight + 'px';

            }
            else {
                containerElement.style.width = cardElement.offsetWidth * numberOfCards + parseInt(cardMargin) * (cardsToLoad - 1) + 'px';
                rowElement.style.width = extra + parseInt(containerElement.style.width.split('px').join('')) * 2 + parseInt(cardMargin) + 'px';
                document.getElementsByClassName('row')[1].style.width = (parseInt(rowElement.style.width.split('px').join('')) / 2) + 'px';

                containerElement.style.height = rowElement.offsetHeight + 'px';

            }

            //Adjust height because of images loading
            for (var i = 0; i < document.getElementsByClassName('card-top-image').length; i++) {
                document.getElementsByClassName('card-top-image')[i].getElementsByTagName('img')[0].onload = function () {
                    containerElement.style.height = rowElement.offsetHeight + 'px';
                };
            }
        }

    }

    /**
     *
     * @param {String} url - URL for GET request
     * @returns {Promise}
     * @description Returns a promise from a GET request
     */
    function getRequest(url) {

        return new Promise(function (resolve, reject) {

            var httpRequest = new XMLHttpRequest();

            httpRequest.onload = function () {
                if (httpRequest.status === 200) {
                    resolve(this);
                }
            };

            httpRequest.onerror = reject;

            httpRequest.open('GET', url, true);
            httpRequest.send();

        });

    }

    /**
     *
     * @param original {Node} Original element to clone
     * @param data {Array} Data to insert in the cloned elements
     * @param whereToStart {int} The index to start in the data object
     * @param howMany {int} How many items to clone
     * @description duplicates items (in this case card items) and appends them to the end
     */
    function duplicateEnd(original, data, whereToStart, howMany) {

        var clone;
        for (var i = whereToStart; i < whereToStart + howMany; i++) {
            clone = cloneElement(original, data[i]);
            original.parentNode.append(clone);
        }
    }

    /**
     *
     * @param {Node} original - Original element to clone
     * @param {Array} data - Data to insert in the cloned elements
     * @param {int} whereToStart - The index to start in the data object
     * @param {int} howMany - How many items to clone
     * @description duplicates items (in this case card items) and appends them to the beginning
     */
    function duplicateBeginning(original, data, whereToStart, howMany) {
        var clone;

        for (var i = whereToStart - 1; i >= whereToStart - howMany; i--) {
            clone = cloneElement(original, data[i]);
            original.parentNode.insertBefore(clone, original.parentNode.firstElementChild);

        }

    }

    /**
     *
     * @param {Node} original - Node to clone and substitute elements
     * @param {Object} data - Object with elements to insert on clone
     * @returns {Node} Returns the Node clone with the new data
     * @description Clone an element, alter it's elements and return the clone
     */
    function cloneElement(original, data) {
        var clone;
        clone = original.cloneNode(true);
        clone.setAttribute('author', data.author);

        clone.getElementsByClassName('card-top-image')[0].getElementsByTagName('img')[0].src = data.image_url + '?$' + Math.random();
        clone.getElementsByClassName('card-titles')[0].getElementsByClassName('card-title')[0].innerHTML = data.title;
        clone.getElementsByClassName('card-text')[0].innerHTML = data.text;
        clone.getElementsByClassName('card-learn-more')[0].href = hrefLink + '/?id=' + data.id;

        return clone;
    }

    /**
     *
     * @param {NodeList} array  - Array to remove elements from
     * @param {int} howMany - How many elements to remove
     * @param {int} [indexToRemove] - Index to remove. Zero to remove the first element. If undefined removes the last element from array
     * @description Remove elements from an array. Remove the first element or the last
     */
    function removeItem(array, howMany, indexToRemove) {

        var position = indexToRemove;

        for (var i = 0; i < howMany; i++) {

            if (indexToRemove === undefined) {
                position = array.length - 1;
            }

            array[position].remove();
        }
    }

    /**
     *
     * @param {String} css - Css selector text to search
     * @param {String} property - Css property we want
     * @returns {String | null} - The property we want from the css SelectorText or null if didn't find the css
     * @description Search the StyleSheets for a CSS SelectorText (e.g. ".container .row") and return the property we want, or null if didn't find the property
     */
    function findPropertyBySelectorText(css, property) {

        var styleSheets = document.styleSheets;

        for (var i = 0; i < styleSheets.length; i++) {
            for (var j = 0; j < styleSheets[i].cssRules.length; j++) {
                if (styleSheets[i].cssRules[j].selectorText === css) {
                    return styleSheets[i].cssRules[j].style[property];
                }
            }
        }

        return null;
    }

    /**
     *
     * @param {String} rule - Name of the Keyframe element we want to search on StyleSheets
     * @returns {Object | null} - Keyframe object we want or null, if didn't find
     * @description Search the StyleSheets for a keyframe name and return the corresponding Object or null, if didn't find object
     */
    function findKeyFrameRule(rule) {

        var styleSheets = document.styleSheets;

        for (var i = 0; i < styleSheets.length; i++) {
            for (var j = 0; j < styleSheets[i].cssRules.length; j++) {
                if (styleSheets[i].cssRules[j].name === rule) {
                    return styleSheets[i].cssRules[j];
                }
            }
        }

        return null;
    }

    /**
     *
     * @param {int} cardsToLoad - Number of cards we are loading
     * @param {int} [extraValue] - The extra value to fix element
     * @returns {int} - Returns value for translateX
     * @description Helper function for translateX of row
     */
    function getTranslateValue(cardsToLoad, extraValue) {
        if (extraValue === undefined) {
            extraValue = 0;
        }

        return -cardMargin * (numberOfCards - 1) + extraValue + cardWidth * cardsToLoad + cardMargin * (document.getElementsByClassName('card').length - 1);
    }

    /**
     *
     * @param {Object} cssElement - Css Keyframe we want to change
     * @param {String} rule - Rule to change (e.g. '0%')
     * @param {int} value - Number of px for keyframe
     * @param {String] property - property for translate keyframe
     * @description Changes a keyframe rule by deleting previous and rewriting a new
     */
    function alterRuleTranslation(cssElement, rule, value, property) {

        cssElement.deleteRule(rule);

        cssElement.appendRule(rule + "{-webkit-transform: translate(" + value + "px);\n" +
            "    -moz-transform: " + property + "(" + value + "px);\n" +
            "    -ms-transform: " + property + "(" + value + "px);\n" +
            "    -o-transform: " + property + "(" + value + "px);\n" +
            "    transform: " + property + "(" + value + "px); }");

    }

})();